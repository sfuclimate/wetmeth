! source file: /home/miklonzo/Git/WETMETH/updates/mtlm_tsi.F
      subroutine mtlm_tsi_def (fname, calendar, expnam, runstamp)

!=======================================================================
!     output routine for land time step integrals

!   inputs:
!     fname      = file name
!     calendar   = calendar
!     expnam     = experiment name
!     runstamp   = run stamp
!=======================================================================

      implicit none

      character(*) :: fname, calendar, expnam, runstamp
      character(3) :: a3
      integer id(1), id_time, iou, ntrec, n

      real c0, c1, c1e20

      c0 = 0.
      c1 = 1.
      c1e20 = 1.e20

!-----------------------------------------------------------------------
!     open file
!-----------------------------------------------------------------------
      call openfile (fname, iou)

!-----------------------------------------------------------------------
!     start definitions
!-----------------------------------------------------------------------
      call redef (iou)

!-----------------------------------------------------------------------
!     write global attributes
!-----------------------------------------------------------------------
      call putatttext (iou, 'global', 'Conventions', 'CF-1.0')
      call putatttext (iou, 'global', 'experiment_name', expnam)
      call putatttext (iou, 'global', 'run_stamp', runstamp)

!-----------------------------------------------------------------------
!     define dimensions
!-----------------------------------------------------------------------
      call defdim ('time', iou, 0, id_time)
      id(1) = id_time

!-----------------------------------------------------------------------
!     define 1d data (t)
!-----------------------------------------------------------------------
      call defvar ('time', iou, 1, id, c0, c0, 'T', 'D'

     &, 'time', 'time', 'years since 0-1-1')

      call putatttext (iou, 'time', 'calendar', calendar)
      call defvar ('T_avgper', iou, 1, id, c0, c0, ' ', 'F'
     &, 'averaging period', ' ','day')

      call defvar ('L_totcarb', iou, 1, id, -c1e20, c1e20, ' ', 'F'
     &, 'global total land carbon', ' ', 'Pg')
      call defvar ('F_carba2l', iou, 1, id, -c1e20, c1e20, ' ', 'F'
     &, 'global total atmopsphere to land carbon flux'
     &, ' ', 'Pg year-1')
      call defvar ('L_soilcarb', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'global total soil carbon', ' ', 'Pg')
      call defvar ('L_soilresp', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total soil respiration flux', ' ', 'Pg yr-1')

      call defvar ('L_wetch4', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global wetland CH4 emissions', ' ', 'Tg C yr-1')
      call defvar ('L_wetch4n40', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'wetland CH4 emissions north of 40N', ' ', 'Tg C yr-1')
      call defvar ('L_wetch4n45', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'wetland CH4 emissions north of 45N', ' ', 'Tg C yr-1')
      call defvar ('L_wetch4n60', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'wetland CH4 emissions north of 60N', ' ', 'Tg C yr-1')
      call defvar ('L_wetch4trop', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'tropical wetland CH4 emissions', ' ', 'Tg C yr-1')

      call defvar ('L_veglit', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total leaf litter flux', ' ', 'Pg yr-1')

      call defvar ('L_vegburn', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total vegetation burning flux', ' ', 'Pg yr-1')
      call defvar ('L_vegcarb', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'global total vegetation carbon', ' ', 'Pg')
      call defvar ('L_vegnpp', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total net primary productivity', ' ', 'Pg yr-1')
      call defvar ('L_veggpp', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total gross primary productivity', ' ', 'Pg yr-1')

      call defvar ('L_veghgt', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global average vegetation height', ' ', 'm')
      call defvar ('L_veglai', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global average leaf area index', ' ', '1')

      call defvar ('L_PFNS45', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Near surface permafrost area  gt 45 N', ' ', 'km2')
      call defvar ('L_PFNH', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Permafrost area  NH', ' ', 'km2')
      call defvar ('L_PFGLOBAL', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Permafrost area  global', ' ', 'km2')
      call defvar ('L_WETNH45', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Total wetlands area gt 45 N', ' ', 'km2')
      call defvar ('L_WETNH45SE', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Seasonal wetlands area gt 45N', ' ', 'km2')
      call defvar ('L_WETGLOBAL', iou, 1, id, c0, c1e20, ' '
     &, 'F', 'Total global wetlands area', ' ', 'km2')

       call defvar ('L_permacarb', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total permafrost carbon pool', ' ', 'Pg')
      call defvar ('L_squedcarb', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'global total sequestered (frozen) carbon', ' ', 'Pg')

      call defvar ('L_PFregcarb', iou, 1, id, -c1e20, c1e20, ' '
     &, 'F', 'permafrost region total soil carbon', ' ', 'Pg')

      call defvar ('L_Zw', iou, 1, id, -c1e20, c1e20, ' ', 'F'
     &,   'water table',' ', 'm')

!-----------------------------------------------------------------------
!     end definitions
!-----------------------------------------------------------------------
      call enddef (iou)

      return
      end

      subroutine mtlm_tsi_out (fname, avgper, time, stamp, CS, RESP_S
     &,                        LIT_C_T, BURN, CV, NPP, GPP, HT, LAI
     &,                        LYING_SNOW, TSOIL, TSTAR

     &,                        PF45, PFNH, PFGLOBAL, WETNH45, WETNH45SE
     &,                        WETGLOBAL, TRACK_PFC, SQU_PFC

     &,                        PREG_C

     &,                        Zw_bar

     &,                        WET_CH4, WET_CH4TROP
     &,                        WET_CH4N40, WET_CH4N45, WET_CH4N60

     &,                        clnd, cfa2l, ntrec)
!=======================================================================
!     output routine for land time step integrals

!   inputs:
!     fname   = file name
!     avgper  = length of averaging period
!     time    = time in years
!     stamp   = time stamp
!     CS, ... = data to be written

!   outputs:
!     ntrec   = number of time record in file
!=======================================================================

      implicit none

      character(*) :: fname, stamp
      character(3) :: a3

      integer iou, ntrec, nyear, nmonth, nday, nhour, nmin, nsec
      integer n

      real CS, RESP_S, LIT_C_T, BURN, CV, NPP, GPP, HT, LAI, LYING_SNOW
      real TSOIL, TSTAR, clnd, cfa2l, avgper, time, tmp
      real c0, c1, C2K, c1e12, kgsPgyr
      real TSTAR_SOIL, PF45, PFNH, PFGLOBAL, WETNH45, WETNH45SE
      real WETGLOBAL, TRACK_PFC, SQU_PFC

      real PREG_C

      real Zw_bar

      real WET_CH4, WET_CH4TROP
      real WET_CH4N40, WET_CH4N45, WET_CH4N60

      real c1e9, kgsTgyr

      c0 = 0.
      c1 = 1.
      C2K = 273.15
      c1e12 = 1.e12
      kgsPgyr = 1.e12/(86400.*365.)

      c1e9 = 1.e9
      kgsTgyr = 1.e9/(86400.*365.)

!-----------------------------------------------------------------------
!     open file and get latest record number
!-----------------------------------------------------------------------
      call opennext (fname, time, ntrec, iou)
      if (ntrec .le. 0) ntrec = 1

!-----------------------------------------------------------------------
!     write 1d data (t)
!-----------------------------------------------------------------------
      call putvars ('time', iou, ntrec, time, c1, c0)
      call rdstmp (stamp, nyear, nmonth, nday, nhour, nmin, nsec)
      call putvars ('T_avgper', iou, ntrec, avgper, c1, c0)

      call putvars ('L_totcarb', iou, ntrec, clnd, c1, c0)
      call putvars ('F_carba2l', iou, ntrec, cfa2l, c1, c0)
      call putvars ('L_soilcarb', iou, ntrec, CS, c1e12, c0)
      call putvars ('L_soilresp', iou, ntrec, RESP_S, kgsPgyr, c0)

      call putvars ('L_wetch4', iou, ntrec, WET_CH4, kgsTgyr, c0)
      call putvars ('L_wetch4n40', iou, ntrec, WET_CH4N40, kgsTgyr, c0)
      call putvars ('L_wetch4n45', iou, ntrec, WET_CH4N45, kgsTgyr, c0)
      call putvars ('L_wetch4n60', iou, ntrec, WET_CH4N60, kgsTgyr, c0)
      call putvars ('L_wetch4trop', iou,ntrec, WET_CH4TROP, kgsTgyr, c0)

      call putvars ('L_veglit', iou, ntrec, LIT_C_T, kgsPgyr, c0)

      call putvars ('L_vegburn', iou, ntrec, BURN, kgsPgyr, c0)
      call putvars ('L_vegcarb', iou, ntrec, CV, c1e12, c0)
      call putvars ('L_vegnpp', iou, ntrec, NPP, kgsPgyr, c0)
      call putvars ('L_veggpp', iou, ntrec, GPP, kgsPgyr, c0)

      call putvars ('L_veghgt', iou, ntrec, HT, c1, c0)
      call putvars ('L_veglai', iou, ntrec, LAI, c1, c0)

      call putvars ('L_PFNS45', iou, ntrec, PF45, c1, c0)
      call putvars ('L_PFNH', iou, ntrec, PFNH, c1, c0)
      call putvars ('L_PFGLOBAL', iou, ntrec, PFGLOBAL, c1, c0)
      call putvars ('L_WETNH45', iou, ntrec, WETNH45, c1, c0)
      call putvars ('L_WETNH45SE', iou, ntrec, WETNH45SE, c1, c0)
      call putvars ('L_WETGLOBAL', iou, ntrec, WETGLOBAL, c1, c0)

      call putvars ('L_permacarb', iou, ntrec, TRACK_PFC, c1e12, c0)
      call putvars ('L_squedcarb', iou, ntrec, SQU_PFC, c1e12, c0)

      call putvars ('L_PFregcarb', iou, ntrec, PREG_C, c1e12, c0)

      call putvars ('L_Zw', iou, ntrec, Zw_bar, c1, c0)

      return
      end
