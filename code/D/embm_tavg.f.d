embm_tavg.f
#if defined O_embm
# if defined O_ice_cpts && defined O_ice
# endif
# if defined O_units_time_years
#  if !defined O_save_time_relyear0
#  else
#  endif
# else
#  if !defined O_save_time_relyear0
#  else
#  endif
# endif
# if defined O_ice_cpts && defined O_ice
# endif
# if !defined O_out_no_L_elev
# endif
# if !defined O_out_no_L_rivers
# endif
# if !defined O_out_no_A_slat
#  if defined O_units_temperature_Celsius
#  else
#  endif
# endif
# if !defined O_out_no_A_shum
# endif
# if !defined O_out_no_A_co2
# endif
# if defined O_N_nox_transport
# endif
# if !defined O_out_no_A_tracer
# endif
# if !defined O_out_no_A_sat
#  if defined O_units_temperature_Celsius
#  else
#  endif
# endif
# if !defined O_out_no_A_precip
# endif
# if !defined O_out_no_F_snow
# endif
# if !defined O_out_no_F_evap
# endif
# if !defined O_out_no_F_rivdis
# endif
# if !defined O_out_no_F_virtual
# endif
# if !defined O_out_no_F_outlwr
# endif
# if !defined O_out_no_F_uplwr
# endif
# if !defined O_out_no_F_upsens
# endif
# if !defined O_out_no_F_dnswr
# endif
# if !defined O_out_no_F_solins
# endif
# if !defined O_out_no_outswr
# endif
# if !defined O_out_no_A_netrad
# endif
# if !defined O_out_no_A_albplt
# endif
# if !defined O_out_no_A_albatm
# endif
# if !defined O_out_no_A_albsur
# endif
# if !defined O_out_no_F_runoff
# endif
# if !defined O_out_no_A_windspd
# endif
# if defined O_save_embm_wind
#  if !defined O_out_no_A_windtX
#  endif
#  if !defined O_out_no_A_windtY
#  endif
#  if !defined O_out_no_A_windqX
#  endif
#  if !defined O_out_no_A_windqY
#  endif
#  if !defined O_out_no_A_windcX
#  endif
#  if !defined O_out_no_A_windcY
#  endif
#  if defined O_N_nox_transport
#  endif
#  if !defined O_out_no_A_wind_tracer_X
#  endif
#  if !defined O_out_no_A_wind_tracer_Y
#  endif
# endif
# if defined O_embm_awind
#  if !defined O_out_no_A_awindX
#  endif
#  if !defined O_out_no_A_awindY
#  endif
#  if !defined O_out_no_A_avgslat
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_A_apress
#  endif
# endif
# if !defined O_mtlm_nsoil && !defined O_out_no_L_soilmois
#endif
# if defined O_units_temperature_Celsius
# else
# endif
# if defined O_save_flxadj
#  if !defined O_out_no_F_adjtemp
#  endif
#  if !defined O_out_no_F_adjsal
#  endif
# endif
# if defined O_save_embm_diff
#  if !defined O_out_no_A_difftX
#  endif
#  if !defined O_out_no_A_diffY
#  endif
#  if !defined O_out_no_A_diffqX
#  endif
#  if !defined O_out_no_A_diffqY
#  endif
#  if !defined O_out_no_A_diffcX
#  endif
#  if !defined O_out_no_A_diffcY
#  endif
#  if defined O_N_nox_transport
#  endif
#  if !defined O_out_no_F_adjsal
#   if !defined O_out_no_A_diff_tracer_X
#  endif
#   if !defined O_out_no_A_diff_tracer_Y
#  endif
# endif
# if defined O_landice_data
#  if !defined O_out_no_L_icefra
#  endif
#  if !defined O_out_no_L_icefra
#  endif
# endif
# if defined O_carbon && defined O_carbon_co2_2d
#  if !defined O_out_no_F_co2
#  endif
#  if defined O_co2emit_data || defined O_co2emit_data_transient
#   if !defined O_out_no_F_co2emit
#   endif
#  endif
# endif
# if defined O_sulphate_data || defined O_sulphate_data_transient
#  if !defined O_out_no_F_sulphfor
#  endif
# endif
# if defined O_ice
#  if !defined O_out_no_O_icetemp
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_O_icethk
#  endif
#  if !defined O_out_no_O_icefra
#  endif
#  if !defined O_out_no_O_snothk
#  endif
# endif
# if defined O_ice_evp && defined O_ice
#  if !defined O_out_no_O_icevelX
#  endif
#  if !defined O_out_no_O_icevelY
#  endif
#  if !defined O_out_no_O_iceintX
#  endif
#  if !defined O_out_no_O_iceintY
#  endif
# endif
# if defined O_ice_cpts && defined O_ice
#  if !defined O_out_no_O_icetempc
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_O_icethkc
#  endif
#  if !defined O_out_no_O_icefrac
#  endif
#  if !defined O_out_no_O_snothkc
#  endif
# endif
# if defined O_save_embm_wind
# endif
# if defined O_embm_awind
# endif
# if !defined O_mtlm_nsoil
# else
# endif
# if defined O_ice
# endif
# if defined O_ice_cpts && defined O_ice
# endif
# if defined O_ice_evp && defined O_ice
# endif
# if defined O_save_flxadj
# endif
# if defined O_save_embm_diff
# endif
# if defined O_landice_data
# endif
# if defined O_carbon_co2_2d
#  if defined O_co2emit_data || defined O_co2emit_data_transient
#  endif
# endif
# if defined O_sulphate_data || defined O_sulphate_data_transient
# endif
# if defined O_save_embm_wind
# endif
# if defined O_embm_awind
# endif
# if defined O_ice
# endif
# if defined O_ice_cpts && defined O_ice
# endif
# if defined O_ice_evp && defined O_ice
# endif
# if defined O_save_flxadj
# endif
# if defined O_save_embm_diff
# endif
# if defined O_landice_data
# endif
# if defined O_carbon_co2_2d
#  if defined O_co2emit_data || defined O_co2emit_data_transient
#  endif
# endif
# if defined O_sulphate_data || defined O_sulphate_data_transient
# endif
# if defined O_ice_cpts && defined O_ice
# endif
# if !defined O_out_no_L_elv
# endif
# if !defined O_out_no_L_rivers
# endif
# if !defined O_out_no_A_slat
#  if defined O_units_temperature_Celsius
#  else
#  endif
# endif
# if !defined O_out_no_A_shum
# endif
# if !defined O_out_no_A_co2
# endif
# if defined O_N_nox_transport
# endif
# if !defined O_out_no_A_tracer
# endif
# if !defined O_out_no_A_sat
#  if defined O_units_temperature_Celsius
#  else
#  endif
# endif
# if !defined O_out_no_F_precip
# endif
# if !defined O_out_no_F_snow
# endif
# if !defined O_out_no_F_evap
# endif
# if !defined O_out_no_F_rivdis
# endif
# if !defined O_out_no_F_virtual
# endif
# if !defined O_out_no_F_outlwr
# endif
# if !defined O_out_no_F_uplwr
# endif
# if !defined O_out_no_F_sens
# endif
# if !defined O_out_no_F_dnswr
# endif
# if !defined O_out_no_F_solins
# endif
# if !defined O_out_no_F_outswr
# endif
# if !defined O_out_no_F_netrad
# endif
# if !defined O_out_no_A_albplt
# endif
# if !defined O_out_no_A_albatm
# endif
# if !defined O_out_no_A_albsur
# endif
# if !defined O_out_no_F_runoff
# endif
# if !defined O_out_no_A_windspd
# endif
#  if defined O_save_embm_wind
#  if !defined O_out_no_A_windtX
#  endif
#  if !defined O_out_no_A_windtY
#  endif
#  if !defined O_out_no_A_windqX
#  endif
#  if !defined O_out_no_A_windqY
#  endif
#  if !defined O_out_no_A_windcX
#  endif
#  if !defined O_out_no_A_windcY
#  endif
#  if defined O_N_nox_transport
#  endif
#  if !defined O_out_no_A_wind_tracer_X
#  endif
#  if !defined O_out_no_A_wind_tracer_Y
#  endif
# endif
# if defined O_embm_awind
#  if !defined O_out_no_A_awindX
#  endif
#  if !defined O_out_no_A_awindY
#  endif
#  if !defined O_out_no_A_avgslat
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_A_press
#  endif
# endif
# if !defined O_mtlm_nsoil && !defined O_out_no_L_soilmois
# endif
# if !defined O_out_no_L_tempsur
#  if defined O_units_temperature_Celsius
#  else
#  endif
# endif
# if defined O_ice
#  if !defined O_out_no_O_icetemp
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_O_icethk
#  endif
#  if !defined O_out_no_O_icefra
#  endif
#  if !defined O_out_no_O_snothk
#  endif
# endif
# if defined O_ice_evp && defined O_ice
#  if !defined O_out_no_O_icevelX
#  endif
#  if !defined O_out_no_O_icevelY
#  endif
#  if !defined O_out_no_O_iceintX
#  endif
#  if !defined O_out_no_O_iceintY
#  endif
# endif
# if defined O_save_flxadj
#  if !defined O_out_no_F_adjtemp
#  endif
#  if !defined O_out_no_F_adjsal
#  endif
# endif
# if defined O_save_embm_diff
#  if !defined O_out_no_difftX
#  endif
#  if !defined O_out_no_A_difftY
#  endif
#  if !defined O_out_no_difftqX
#  endif
#  if !defined O_out_no_A_diffqY
#  endif
#  if !defined O_out_no_A_diffcX
#  endif
#  if !defined O_out_no_A_diffcY
#  endif
#  if defined O_N_nox_transport
#  endif
#  if !defined O_out_no_A_diff_tracer_X
#  endif
#  if !defined O_out_no_A_diff_tracer_Y
#  endif
# endif
# if defined O_landice_data
#  if !defined O_out_no_L_icefra
#  endif
#  if !defined O_out_no_L_icethk
#  endif
# endif
# if O_carbon_co2_2d
#  if !defined O_out_no_F_co2
#  endif
#  if defined O_co2emit_data || defined O_co2emit_data_transient
#   if !defined O_out_no_F_co2emit
#   endif
#  endif
# endif
# if defined O_sulphate_data || defined O_sulphate_data_transient
#  if !defined O_out_no_A_sulphfor
#  endif
# endif
# if defined O_ice_cpts && defined O_ice
#  if !defined O_out_no_O_icetempc
#   if defined O_units_temperature_Celsius
#   else
#   endif
#  endif
#  if !defined O_out_no_O_icethkc
#  endif
#  if !defined O_out_no_O_icefrac
#  endif
#  if !defined O_out_no_O_snothkc
#  endif
# endif
#endif
