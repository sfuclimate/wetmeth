! source file: /home/miklonzo/Git/WETMETH/updates/nitrodep.h
! ==================== include file nitrodep.h ====================

! Parameters used for Nitrogen deposition

! Variables for N deposition forcing
!----------------------------------------------------------------------
! noy   = NOy deposition data (mg N/m2/yr).
! nhx   = NHx deposition data (mg N/m2/yr).
! nitro_yr = year for nitrogen deposition (1860 to 2050)

      real noy, nhx, nitro_yr

      common /soil_r/ noy(imt,jmt,1:3), nhx(imt,jmt,1:3)
