! source file: /home/miklonzo/UVic_ESCM/2.9/source/mom/diaga.h
!====================== include file "diaga.h" =========================

!     variables used for computing diagnostics:

!     totalk   = total number of levels involved in convection
!     vdepth   = ventilation depth (cm)
!     pe       = potential energy lost due to explicit convection (g/s2)

      real totalk, vdepth, pe
      common /cdiaga_r/ totalk(imt,jsmw:jemw), vdepth(imt,jsmw:jemw)
      common /cdiaga_r/ pe(imt,jsmw:jemw)

!     sspH   = sea surface pH
!     ssCO3  = sea surface CO3 concentration
!     ssOc   = sea surface Omega_c
!     ssOa   = sea surface Omega_a
!     sspCO2 = sea surface pCO2

      real sspH, ssCO3, ssOc, ssOa, sspCO2

      common /cdiaga_r/ sspH(imt,jmt), ssCO3(imt,jmt), ssOc(imt,jmt)
      common /cdiaga_r/ ssOa(imt,jmt), sspCO2(imt,jmt)
