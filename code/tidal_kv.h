! source file: /home/miklonzo/UVic_ESCM/2.9/source/mom/tidal_kv.h
!===================== include file "tidal_kv.h" =======================

!     quantities for parameterization of tidal mixing

!     Simmons, H.L., S.R. Jayne, L.C. St. Laurent and A.J. Weaver, 2004:
!     Tidally driven mixing in a numerical model of the ocean general
!     circulation. Ocean Modelling, 6, 245-263.

