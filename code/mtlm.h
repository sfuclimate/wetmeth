! source file: /home/miklonzo/Git/WETMETH/updates/mtlm.h
!======================== include file "mtlm.h" ========================

!**********************************************************************
! this file is based on code that may have had the following copyright:
! (c) CROWN COPYRIGHT 1997, U.K. METEOROLOGICAL OFFICE.

! Permission has been granted by the authors to the public to copy
! and use this software without charge, provided that this Notice and
! any statement of authorship are reproduced on all copies. Neither the
! Crown nor the U.K. Meteorological Office makes any warranty, express
! or implied, or assumes any liability or responsibility for the use of
! this software.
!**********************************************************************

!-----------------------------------------------------------------------
! Atmospheric CO2 variables
!-----------------------------------------------------------------------
! CO2   = CO2 mass mixing ratio (kg/kg).
! EPCO2 = Ratio of molecular weights of CO2

      real EPCO2, CO2
      parameter (EPCO2 = 1.5194)

      common /land_r/ CO2(POINTS)

!-----------------------------------------------------------------------
! Driving variables
!-----------------------------------------------------------------------
! LYING_SNOW = Snow mass (kg/m2).
! LW         = Surface longwave radiation (W/m**2).
! SW         = Surface shortwave radiation (W/m**2).
! SWN        = Net shortwave radiation (W/m**2).
! PSTAR      = Surface pressure (Pa).
! Q          = Specific humidity (kg/kg).
! SAT_D      = Diurnal surface air temperature (K).
! TS1        = Sub-surface temperature (K).
! TIMEDAY    = Time of day (s).
! W          = Water incident at the soil surface (mm/day).
! WIND       = Wind speed (m/s).
! LW_OUT     = total net longwave radiation (W/m2)
! LHC        = Latent heat of condensation (J/kg).
! LHF        = Latent heat of fusion (J/kg).
! SIGMA      = Stefan-Boltzman constant (W/m2/K4).

      real LYING_SNOW, LW, SW, SWN, PSTAR, Q, SAT_D, TS1, TIMEDAY
      real W, WIND, LW_OUT, LHC, LHF, SIGMA

      common /land_r/ LYING_SNOW(POINTS), LW(POINTS), SW(POINTS)
      common /land_r/ SWN(POINTS), PSTAR(POINTS), Q(POINTS)
      common /land_r/ SAT_D(POINTS), TS1(POINTS), TIMEDAY
      common /land_r/ WIND(POINTS), LW_OUT(POINTS), LHC, LHF, SIGMA

!-----------------------------------------------------------------------
! Variables for forcing with climatological means
!-----------------------------------------------------------------------
! DTEMP_DAY    = Diurnal temperature range (K).
! QS           = Saturated specific humidity (kg/kg).
! RAIN         = Rainfall rate (kg/m2/s).
! SNOW         = Snowfall rate (kg/m2/s).
! SNOWMELT     = Snow melt (mm/day).
! SW_C         = Surface shortwave radiation (W/m**2).
! SUN          = Normalized solar radiation.
! SURF_ROFF    = Surface runoff (mm/day).
! TIME_MAX     = GMT at which maximum temperature occurs (s).
! T_C          = Air temperature (K).
! W_C          = Water incident at the soil surface (mm/day).
! RH_C         = relative humidity from atmospheric model

      real DTEMP_DAY, QS, RAIN, SNOW, SNOWMELT, SW_C, SUN, SURF_ROFF
      real TIME_MAX, T_C, W_C, RH_C

      common /land_r/ DTEMP_DAY(POINTS), QS(POINTS), RAIN(POINTS)
      common /land_r/ SNOW(POINTS), SNOWMELT(POINTS), SW_C(POINTS)
      common /land_r/ SUN(POINTS,STEPSM), SURF_ROFF(POINTS)
      common /land_r/ TIME_MAX(POINTS), T_C(POINTS), W_C(POINTS)
      common /land_r/ RH_C(POINTS)

!-----------------------------------------------------------------------
! Model parameters
!-----------------------------------------------------------------------
! TIMESTEP    = Timestep for daily calculations (s).

      real TIMESTEP
      common /land_r/ TIMESTEP

!-----------------------------------------------------------------------
! Soil parameters
!-----------------------------------------------------------------------
! ALBSOIL   = Soil albedo.
! ALBSNOW   = Snow albedo.
! ALBLAND   = Surface albedo.
! Z0S       = Roughness length for bare soil (m).
! ROOTDEP   = Rootdepth (m).
! HCAP_SOIL = Soil heat capacity (W/m3/K).
! HCON_SOIL = Soil heat conductivity (W/m/K).
! HCON_D_P  = Dry soil thermal conductivity (W/m/K)
! HCON_SNOW  = Snow thermal conductivity (W/m/K)

      real ALBSOIL, ALBSNOW, ALBLAND, Z0S, HCAP_SOIL, HCON_SOIL

!      parameter (HCAP_SOIL=3.3E5, HCON_SOIL=0.23)
      parameter (HCAP_SOIL=3.3E5, HCON_SOIL=0.75)

      real HCON_D_P,HCON_SNOW

      common /land_r/ ALBSOIL(POINTS), ALBSNOW(POINTS)
      common /land_r/ ALBLAND(POINTS), Z0S(POINTS)
      common /land_r/ HCON_D_P,HCON_SNOW

!-----------------------------------------------------------------------
! Vegetation parameters
!-----------------------------------------------------------------------
! ALBSNC    = Cold deep snow albedo.
! ALBSNF    = Snow free albedo.
! CATCH     = Canopy capacity (kg/m2).
! Z0        = Vegetative roughness length (m).
! VEG_FRAC  = Vegetated fraction.
! FRAC_VS   = Total fraction of gridbox covered by vegetation and soil.
! FRAC_MIN  = Minimum areal fraction for PFTs.
! FRAC_SEED = "Seed" fraction for PFTs.

      real ALBSNC, ALBSNF, CATCH, Z0, VEG_FRAC, FRAC_VS, FRAC_MIN
      real FRAC_SEED
      parameter (FRAC_MIN=1.0E-6, FRAC_SEED=0.01)

      common /land_r/ ALBSNC(POINTS,NPFT), ALBSNF(POINTS,NPFT)
      common /land_r/ CATCH(POINTS,NPFT)
      common /land_r/ Z0(POINTS,NPFT), VEG_FRAC(POINTS)
      common /land_r/ FRAC_VS(POINTS)

!-----------------------------------------------------------------------
! IVM Prognostics
!-----------------------------------------------------------------------
! CS   = Soil carbon (kg C/m2).
! FRAC = Areal coverage.
! LAI  = Leaf area index.

      real CS, FRAC, LAI

      common /land_r/ CS(POINTS), FRAC(POINTS,NTYPE)
      common /land_r/ LAI(POINTS,NPFT)

!-----------------------------------------------------------------------
! IVM Fluxes and diagnostics
!-----------------------------------------------------------------------
! C_VEG       = Vegetation carbon (kg C/m2).
! CV          = Gridbox mean vegetation carbon (kg C/m2).
! FTIME       = Weighting factor for accumulations.
! HT          = Canopy height (m).
! GPP         = Gross Primary Productivity (kg C/m2/s).
! G_LEAF      = Leaf turnover rate (/yr).
! G_LEAF_DAY  = Daily mean leaf turnover rate (/yr).
! G_LEAF_PHEN = Daily leaf turnover rate including phenology (/yr).
! NPP         = Net Primary Productivity (kg C/m2/s).
! RESP_S      = Soil respiration rate (kg C/m2/s).
! LIT_C_T     = Gridbox mean carbon litter (kg C/m2/yr).
! BF          = Burn fraction

! PGND_CH4D   = Methanogenesis rates per regular soil layer (kg C/m3/s)
! WET_CH4     = Wetland methane emissions out of the soil column (kg C/m2/s)

! PGND_CH4P   = Methanogenesis rates per permafrost layer (kg C/m3/s)

      real C_VEG, CV, FTIME, HT, GPP, G_LEAF, G_LEAF_DAY
      real G_LEAF_PHEN, NPP, RESP_S, LIT_C_T, BF

      real PGND_CH4D
      real WET_CH4

      real PGND_CH4P

      common /land_r/ C_VEG(POINTS,NPFT), CV(POINTS), FTIME
      common /land_r/ HT(POINTS,NPFT), GPP(POINTS,NPFT)
      common /land_r/ G_LEAF(POINTS,NPFT), G_LEAF_DAY(POINTS,NPFT)
      common /land_r/ G_LEAF_PHEN(POINTS,NPFT), NPP(POINTS,NPFT)
      common /land_r/ RESP_S(POINTS), LIT_C_T(POINTS), BF

      common /land_r/ PGND_CH4D(POINTS,NGND)
      common /land_r/ WET_CH4(POINTS)

      common /land_r/ PGND_CH4P(POINTS,NGND)

!-----------------------------------------------------------------------
! Driving variables for TRIFFID
!-----------------------------------------------------------------------
! G_LEAF_DR  = Accumulated leaf turnover rate (/yr).
! NPP_DR     = Accumulated Net Primary Productivity (kg C/m2/yr).
! RESP_S_DR  = Accumulated soil respiration rate (kg C/m2/yr).
! RESP_W_DR  = Accumulated wood respiration rate (kg C/m2/yr).

      real G_LEAF_DR, NPP_DR, RESP_S_DR, RESP_W_DR

      common /land_r/ G_LEAF_DR(POINTS,NPFT), NPP_DR(POINTS,NPFT)
      common /land_r/ RESP_S_DR(POINTS), RESP_W_DR(POINTS,NPFT)

!-----------------------------------------------------------------------
! Hydrology variables
!-----------------------------------------------------------------------
! ESUB       = Sublimation (kg/m2/s).
! FSMC       = Moisture availability factor.
! MAF        = Moisture availability factor PFT/soil dependence
! M          = Total soil moisture (kg/m2).
! MNEG       = Negative soil moisture (kg/m2).

      real ESUB, FSMC, MAF, MNEG

      common /land_r/ ESUB(POINTS), FSMC(POINTS), MAF(NTYPE)

      common /land_r/ MNEG(POINTS)

!-----------------------------------------------------------------------
! Temperatures
!-----------------------------------------------------------------------
! TSOIL = Temperature of bare soil (K).
! TSTAR = Surface temperature (K).

      real TSOIL, TSTAR

      common /land_r/ TSOIL(POINTS), TSTAR(POINTS,NPFT)

!-----------------------------------------------------------------------
! Inputs defining locations
!-----------------------------------------------------------------------

! LAND_PTS   = Number of land points.
! LAND_INDEX = Indices of land points.
! VEG_PTS    = Number of land points which include the nth vegetation
!              type.
! VEG_INDEX  = Indices of land points which include the nth vegetation
!              type.

      integer LAND_PTS, LAND_INDEX, VEG_PTS, VEG_INDEX

      common /land_i/ LAND_PTS, LAND_INDEX(POINTS), VEG_PTS(NPFT)
      common /land_i/ VEG_INDEX(POINTS,NPFT)

! LAT             = Latitude (degrees)
! LATMIN,LATMAX   = Latitudinal limits of the area (degrees).
! GAREA           = grid area (m2)
! LONG            = Longitude (degrees)
! LONGMIN,LONGMAX = Longitudinal limits of the area (degrees).

      real LAT, LATMIN, LATMAX, LONG, LONGMIN, LONGMAX, GAREA

      common /land_r/ LAT(POINTS),LATMIN, LATMAX, GAREA(POINTS)
      common /land_r/ LONG(POINTS), LONGMIN, LONGMAX

!-----------------------------------------------------------------------
! Time parameters
!-----------------------------------------------------------------------
! ISTEP        = step counter.
! STEP_DAY     = Number of steps in a day.
! LAND_COUNTER = number time steps for the model.

      integer ISTEP, STEP_DAY, LAND_COUNTER

      common /land_i/ ISTEP, STEP_DAY, LAND_COUNTER

! SEC_DAY  = Number of seconds in a day (s).
! DAY_YEAR = Number of days in a year (days).
! SEC_YEAR = Number of seconds in a year (s).

      real DAY_YEAR, SEC_DAY, SEC_YEAR
      common /land_r/ DAY_YEAR, SEC_DAY, SEC_YEAR

! INT_VEG   = .T. for interactive vegetation
! VEG_EQUIL = .T. if the vegetation equilibrium

      logical INT_VEG, VEG_EQUIL

      common /land_l/ INT_VEG, VEG_EQUIL

!-----------------------------------------------------------------------
! Variables defining anthropogenic disturbance
!-----------------------------------------------------------------------
! FRACA     = Areal fraction of agriculture.
! G_ANTH    = Anthropogenic disturbance rate (/yr).

      real FRACA, G_ANTH

      common /land_r/ FRACA(POINTS), G_ANTH(POINTS)

!-----------------------------------------------------------------------
! New variables required for LAND module
!-----------------------------------------------------------------------
! ET       = Evapotranspiration (kg/m2/s).
! LE       = Latent heat flux (W/m2).
! TSTAR_GB = Grid box average surface temperature (K).
! SH       = Sensible heat flux (W/m2).

      real ET, LE, TSTAR_GB, SH

      common /land_r/ ET(POINTS), LE(POINTS), TSTAR_GB(POINTS)
      common /land_r/ SH(POINTS)

! DAY_PHEN = IN Number of days between phenology.
! DAY_TRIF = IN Number of days between TRIFFID.

      integer  DAY_PHEN, DAY_TRIF

      common /land_i/ DAY_PHEN, DAY_TRIF

! L_PHEN = IN .T. if phenology to be updated.
! L_TRIF = IN .T. if vegetation to be updated.

      logical L_PHEN, L_TRIF

      common /land_l/ L_PHEN, L_TRIF

!-----------------------------------------------------------------------
! Spatial characteristics of grid cell
!-----------------------------------------------------------------------

! DZ_GND = Thickness of soil + rock layers
! NSOIL  = Number of soil layers in a particular gridcell (default 8)
! ZTOP   = Depth of top of soil layer
! ZBOT   = Depth of bottom of soil layer

      integer NSOIL

      real DZ_GND(NGND)
      real ZTOP, ZBOT

      common /land_r/ ZTOP(NGND), ZBOT(NGND)
      common /land_i/ NSOIL(POINTS)

      DATA DZ_GND / 0.1, 0.17, 0.29, 0.50, 0.85, 1.44, 2.46, 4.19
     &,                7.14, 12.18, 20.77, 35.42, 60.39, 104.4/

!-----------------------------------------------------------------------
! Constants
!-----------------------------------------------------------------------
! DPSIDT    = Rate of change of ice potential w/T
!             RHO_I*LHF/(ZERODEGC*RHO_W_G)
! HCAP_I    = Specific heat capacity of ice (J/kgK)
! HCAP_ROCK = Specific heat capacity of rock (J/kgK)
! HCAP_W    = Specific heat capacity of water (J/kgK)
! HCON_ROCK = Thermal conductivity of rock (W/mK)
! RHO_I     = Density of ice (kg/m3)
! RHO_S     = Density of snow (kg/m3)
! RHO_W     = Density of pure water (kg/m3)
! Z0_SOIL   = Roughness length for bare soil (m).
! ZERODEGC  = Celcius to Kelvin conversion

      real HCAP_I, HCAP_W, DPSIDP, Z0_SOIL
      real ZERODEGC
      real RHO_I, RHO_S, RHO_W, DPSIDT
      real HCAP_ROCK, HCON_ROCK

      parameter (ZERODEGC = 273.15)
      parameter (RHO_W  = 1000., RHO_I = 917., RHO_S = 330.)
      parameter (Z0_SOIL = 0.0003)
      parameter (HCAP_W = 4180.0, HCAP_I = 2100.0, DPSIDT = 114.3)
      parameter (HCAP_ROCK = 2400000 , HCON_ROCK = 3.4)

!-----------------------------------------------------------------------
! Snow characteristics
!-----------------------------------------------------------------------
! SNOW_HT = Height of lying snow(m)
! DSNOW   = Number of days out of the year that lying snow was present
!
      real SNOW_HT, DSNOW

      common /land_r/ SNOW_HT(POINTS), DSNOW(POINTS)

!-----------------------------------------------------------------------
! Moisture characteristics
!-----------------------------------------------------------------------
! SUBSNOW = Sublimation from lying snow (total gridcell) (kg/m2/s)
! SUBSOIL = Sublimation from frozen soil (kg/m2/s)

      real SUBSNOW, SUBSOIL

      common /land_r/ SUBSNOW(POINTS),SUBSOIL(POINTS)

!-----------------------------------------------------------------------
! Temperatures and Energy Fluxes
!-----------------------------------------------------------------------
! TSTAR_S = Bare soil skin temperature (K).
! TSTAR_P = PFT skin temperature (K).

      real TSTAR_S, TSTAR_P

      common /land_r/ TSTAR_S(POINTS), TSTAR_P(POINTS,NPFT)

!-----------------------------------------------------------------------
! Soil moisture parameters
!-----------------------------------------------------------------------
! B         = B parameter in Clapp-Hornberger parameterization
! H2OHEAT  = Energy that needs to be added to / removed from soil column
!           = to account for heat capacity of water moving through the soil
! F_ROOT    = Fraction of roots in the nth soil layer as a function of PFT
! KS        = Saturated hydraulic conductivity (kg/m2s)
! M         = Total soil moisture (kg/m2)..
! PHIS      = Saturated soil water suction (m)
! SF        = Frozen soil moisture as a fraction of saturation
! SU        = Unfrozen soil moisture as a fraction of saturation
! V_CRIT    = Critical volumetric soil moisture
! V_SAT     = Saturated volumetric soil moisture
! V_WILT    = Wilting volumetric soil moisture

      real KS, PHIS, SU, SF, V_CRIT
      real V_WILT, F_ROOT, B
      real M
      real H2OHEAT,  V_SAT

      common /land_r/ B(POINTS,NGND), KS(POINTS,NGND)
      common /land_r/ PHIS(POINTS,NGND)
      common /land_r/ F_ROOT(POINTS,NGND,NPFT)
      common /land_r/ SF(POINTS,NGND), SU(POINTS,NGND)
      common /land_r/ V_CRIT(POINTS,NGND)
      common /land_r/ V_WILT(POINTS,NGND), H2OHEAT(POINTS)
      common /land_r/ V_SAT(POINTS,NGND)
      common /land_r/ M(POINTS,NGND)

!-----------------------------------------------------------------------
! Soil thermal parameters
!-----------------------------------------------------------------------
! GEOHEAT    = Geothermal heat flux (W/m2)
! HCAP_D     = Dry soil heat capacity (J/m3/K).
! HCON_B     = Thermal conductivity at layer boundary
!              (0 = surface, 1 = bottom of layer 1, etc...)
! HCON_D     = Dry soil thermal conductivity (W/m/K).
! SPINUPTEMP = Temperature to initialize subsurface layers to if using
!              quick spinup option (K)
! TGND       = Sub-surface temperature (K).

      real GEOHEAT, HCAP_D, HCON_B
      real HCON_D,SPINUPTEMP
      real TGND

      common /land_r/ GEOHEAT(POINTS), HCAP_D(POINTS,NGND)
      common /land_r/ HCON_B(POINTS,NGND), HCON_D(POINTS,NGND)
      common /land_r/ SPINUPTEMP(POINTS)
      common /land_r/ TGND(POINTS,NGND)

!----------------------------------------------------------------------
! Wetlands parameters
!-----------------------------------------------------------------------
! ANN_DWET  = Number of days cell has met the wetlands moisture criteria
!           = updated once per year
! DWET      = Number of days the gridcell has met the wetlands moisture
!             criteria
! WETMAP    = Fraction of a gridcell covered by wetlands at a given time
!           = WETMAP(:,1) = Total wetlands
!           = WETMAP(:,2) = Peristant wetlands (DWET < 365)
!           = WETMAP(:,3) = Seasonal wetlands (DWET > 365)
! SLOPEVAL  = Values of slope corresponding to SLOPEMAP
! SLOPEMAP  = Fraction of grid cell having slope less than corresponding SLOPEVAL
! YEARCOUNT = Counter for reassessing DWET and WETMAP
!
      real SLOPEVAL, SLOPEMAP, DWET, WETMAP, YEARCOUNT
      real ANN_DWET

      common /land_r/ DWET(POINTS), WETMAP(POINTS,3)
      common /land_r/ ANN_DWET(POINTS)
      common /land_r/ YEARCOUNT
      common /land_r/ SLOPEVAL(NSLOPE), SLOPEMAP(POINTS, NSLOPE)

!--------------------------------------------------------------------------------------
!~Top MODEL PARAMETERS~
!--------------------------------------------------------------------------------------
! See Gedney and Cox 2003 for details

! alphaTOP    = alpha parameter for gamma distribution of topographic index
! betaTOP     = beta parameter for gamma distribution of topographic index
! shiftTOP    = shift parameter for gamma distribution of topographic index
! Zw_bar      = grid cell average water table depth
! Zw_eff      = effective grid cell average water table depth
! Zw_max      = yearly maxium grid cell average water table
! TOPidx_bar  = mean topographic index
! TransZ      = Transmissivity to top of each carbon containing layer T(0)
! GammaFunc   = Gamma function values for each grid point
! TOPidx_crt  = Maximum topindex before flowing water
! Qflow       = ground water flow in each layer

      real alphaTOP, betaTOP, shiftTOP, Zw_bar, TOPidx_bar
      real TransZ, GammaFunc, TOPidx_crt, Zw_eff, Zw_max
      real Qflow

      parameter(TOPidx_crt=9.0)  !9.5 !25.0

      common /land_r/ alphaTOP(POINTS), betaTOP(POINTS)
      common /land_r/ shiftTOP(POINTS), Zw_bar(POINTS)
      common /land_r/ TOPidx_bar(POINTS), TransZ(POINTS,SCHDMAX)
      common /land_r/ GammaFunc(POINTS), Zw_eff(POINTS)
      common /land_r/ Zw_max(POINTS), Qflow(POINTS, NGND)

      integer SW_Zw
      common /land_i/ SW_Zw

!-----------------------------------------------------------------------
! Standard permafrost parameters
!-----------------------------------------------------------------------
! ACTIVELT    = Active layer thickness (m)
! DFROZEN_A   = Days out of past year ground has been frozen
! DFROZEN_C   = Days since ground first froze - cumulative total
! DUNFROZEN_C = Days since ground thawed
! FROZENMAP   = Map of permafrost / frozen ground / snow covered terrain!
!             = 1. Permafrost distribution (Dfrozen  > 730)
!             = 2. Seasonally frozen ground (730 >= Dfrozen > 15)
!             = 3. Intermittently frozen ground ( 15 >= Dfrozen >= 1)
!             = 4. Diurnally frozen ground (1 > Dfrozen > 0)
!             = 5. Seasonally snow covered ground (Dsnow > 15)
!             = 6. Intermittently snow covered ground (15 >= Dsnow >= 1)
!             = 7. Diurnally snow covered ground (1> Dsnow >0)
! PERMA       = Depth to permafrost (m)
! PFTHICK     = Thickness of PF layer (m)
! TALIK       = Thickness of taliks (if present) (m)

      real PERMA,DFROZEN_A, DFROZEN_C, PFTHICK,ACTIVELT,TALIK
      real DUNFROZEN_C
      real FROZENMAP

      common /land_r/ FROZENMAP(POINTS, 7), DFROZEN_A(POINTS,NGND)
      common /land_r/ DFROZEN_C(POINTS,NGND), PERMA(POINTS)
      common /land_r/ PFTHICK(POINTS), ACTIVELT(POINTS)
      common /land_r/ TALIK(POINTS), DUNFROZEN_C(POINTS,NGND)

!-----------------------------------------------------------------------
! Excess ice scheme parameters
!-----------------------------------------------------------------------
! EXCESSI   = Excess ground ice content (%)
! EXCOL     = Column integrated excess ice percentage (%)
! LAYERICE  = Excess ice content in  soil layer
! ISEXSLAB  = Does the grid cell still have slab excess ice?
! SUBSIDE   = Degree of subsidence since model initialization (m)
! TSLAB     = Temperature in slab ice model (K)
! MSLAB     = Mass of ice in slab PF layer (kg/m2)

      real EXCESSI, SUBSIDE, TSLAB, MSLAB
      real EXCOL, LAYERICE
      logical :: ISEXSLAB

      common /land_r/ EXCESSI(POINTS), ISEXSLAB(POINTS)
      common /land_r/ SUBSIDE(POINTS), EXCOL(POINTS)
      common /land_r/ TSLAB(POINTS), MSLAB(POINTS)
      common /land_r/ LAYERICE(POINTS,NGND)

!-----------------------------------------------------------------------
! Schafer algorithm variables
!-----------------------------------------------------------------------
! SCHDMIN   = active carbon depth tracer (layer #)
! SCHON     = flag to examin if function is working
! SCH_REST  = Time to reset SCHDMIN during spinup (days)
! SCH_COUNT = Counts time steps for SCH_REST
! RESP_SD   = Soil respiration rate in each layer (kg C/m3/s).
! C_WEIGHT  = Fraction of total soil organic content in a layer
!           = used in weighting soil respiration in microbe.F
! CS_D      = Volumetric density of soil carbon in each layer (kg C/m3)
! RESP_S_DD = Accumulated soil respiration rate
!           = in each layer (kg C/m3/yr)
! DMIN_HOLD = holds SCHDMIN from last triffid time-step
! PFCDENSE  = permafrost carbon density (kg C/m3)
! TRACK_PFC = tracks permafrost carbon (kg C/m2)
! SQU_PFC   = sequestered permafrost carbon (kg C/m2)
! FZN_SL    = frozen soil layers (layer #)

      integer SCHON, SCH_REST, SCH_COUNT
      real SCHDMIN, RESP_S_DD, RESP_SD
      real C_WEIGHT, CS_D, DMIN_HOLD, PFCDENSE
      real TRACK_PFC, SQU_PFC, FZN_SL

      common /land_r/ SCHDMIN(POINTS), SCHON, SCH_COUNT
      common /land_r/ CS_D(POINTS,NGND), C_WEIGHT(POINTS,NGND)
      common /land_r/ RESP_S_DD(POINTS,NGND), RESP_SD(POINTS,NGND)
      common /land_r/ DMIN_HOLD(POINTS), TRACK_PFC(POINTS)
      common /land_r/ SQU_PFC(POINTS), FZN_SL(POINTS)

      parameter (SCH_REST = 36500, PFCDENSE = 21.0)

!-----------------------------------------------------------------------
! Koven diffusion variables
!-----------------------------------------------------------------------
! KOVDIFF_P  = Parameters for carbon diffusion (m-2)
! YEARKOV_SW = Swithch for year count
! KOVCON     = Constant of diffution (m2a-1)
! KOVMAX   = Maximum depth of mixing KOVMAX*active layer thickness
! ZMID   = Depth of midpoint of soil layer
! KOVSAT = Controls permafrost carbon saturation
! KAPSp = Specific soil respiration rate at 25 deg and optimum soil
!        moisture (/s) for permafrost carbon.

      integer YEARKOV_SW

      common /land_i/ YEARKOV_SW

      real KOVDIFF_P,KOVCON,KOVMAX,ZMID,KOVSAT, KAPSp

      common /land_r/ KOVDIFF_P(SCHDMAX,3),ZMID(NGND)
      common /land_r/ KOVCON,KOVSAT, KAPSp

      parameter(KOVMAX=4.0)

!-----------------------------------------------------------------------
! Permafrost carbon pool variables
!-----------------------------------------------------------------------
! CS_P = Depth varying permafrost carbon density (kg m-3)
! RESP_SP = Permafrost carbon respiration rate in each layer (kg C/m3/s).
! RESP_S_DP = Accumulated permafrost respiration rate
! PPtm = passive pool transmutation rate
! iAF = Initial available fraction
! AFp = Available fraction permafrost carbon
! AF_DTa = Available fraction transmutation accumulator
! AF_Ta = Available fraction transmutation rate
! PREG_C = Soil carbon in permafrost region both in permafrost and active layer

      real CS_P, RESP_SP, RESP_S_DP
      real PPtm, iAF, AFp, AF_Ta, AF_DTa,PREG_C

      common /land_r/ CS_P(POINTS,NGND)
      common /land_r/ RESP_S_DP(POINTS,NGND), RESP_SP(POINTS,NGND)
      common /land_r/ PPtm, iAF, AFp(POINTS,NGND),AF_Ta(POINTS,NGND)
      common /land_r/ AF_DTa(POINTS,NGND),PREG_C(POINTS)

!-----------------------------------------------------------------------
! Wetland CH4 variables: used in order to remove wetland CH4 from soil C
!-----------------------------------------------------------------------
! These variables are needed to update the soil C following CH4 production
! Relevant subroutines: mtlm.F, setmtlm.F, soilcarb.F, triffid.F
!-----------------------------------------------------------------------
! PGND_CH4DD = Accumulated CH4 production from regular soil C per soil layer
! PGND_CH4PP = Accumulated CH4 production from permafrost soil C per soil layer

      real PGND_CH4DD, PGND_CH4PP

      common /land_r/ PGND_CH4DD(POINTS,NGND), PGND_CH4PP(POINTS,NGND)

! In order to use SAT_FRAC in wetland methane processes
! (refer to the 1 flag/option), when soil
! nitrogen processes are NOT considered

! SAT_FRAC    = Saturated fraction of each grid cell

      real SAT_FRAC
      common /land_r/ SAT_FRAC(POINTS,SCHDMAX)

!-----------------------------------------------------------------------
! Soil carbon partitioning variables
!-----------------------------------------------------------------------
! CS_PART = partitioning of Litter into different layers

      real CS_PART

      common /land_r/ CS_PART(SCHDMAX+1,NGND)

!-----------------------------------------------------------------------
! Vegetation parameters and fluxes
!-----------------------------------------------------------------------
! HT_S = Height variable for soil (0 everywhere)

       real HT_S

       common /land_r/ HT_S(POINTS)

!-----------------------------------------------------------------------
!  Moisture parameters
!-----------------------------------------------------------------------
! FSMC_PFT   = Moisture availability factor
! INFILFAC  = Infiltration enhancement factor
! ROOTDEP   = Vegetation rooting depth (m)

      real FSMC_PFT, INFILFAC, ROOTDEP

      common /land_r/ FSMC_PFT(POINTS,NPFT), INFILFAC(POINTS, NPFT)
      common /land_r/ ROOTDEP(POINTS, NPFT)

!-----------------------------------------------------------------------
! Carbon variables
!-----------------------------------------------------------------------
! LIT_C      = Carbon litter (kg C/m2/yr)

      real LIT_C

      common /land_r/ LIT_C(POINTS, NPFT)

!-----------------------------------------------------------------------
! time averaged arrays
!-----------------------------------------------------------------------
! ntatsl        = time step counter for time averaging
! ta_TS1        = time averaged TS1
! ta_TSTAR      = time averaged TSTAR
! ta_ALBLAND    = time averaged ALBLAND
! ta_ET         = time averaged ET
! ta_M          = time averaged M
! ta_CS         = time averaged CS
! ta_RESP_S     = time averaged RESP_S
! ta_LIT_C_T    = time averaged LIT_C_T
! ta_BURN       = time averaged BURN
! ta_GPP        = time averaged GPP
! ta_NPP        = time averaged NPP
! ta_HT         = time averaged HT
! ta_LAI        = time averaged LAI
! ta_C_VEG      = time averaged C_VEG
! ta_LYING_SNOW = time averaged LYING_SNOW
! ta_SURF_ROFF  = time averaged SURF_ROFF
! ta_FRAC       = time averaged FRAC

      integer ntatsl

      common /land_i/ ntatsl

      real ta_TS1, ta_TSTAR_GB, ta_ALBLAND, ta_ET, ta_M, ta_CS
      real ta_RESP_S, ta_LIT_C_T, ta_BURN, ta_GPP, ta_NPP, ta_HT
      real ta_LAI, ta_C_VEG, ta_LYING_SNOW, ta_SURF_ROFF, ta_FRAC

      common /land_r/ ta_TS1(POINTS), ta_TSTAR_GB(POINTS)
      common /land_r/ ta_ALBLAND(POINTS), ta_ET(POINTS), ta_M(POINTS)
      common /land_r/ ta_CS(POINTS), ta_RESP_S(POINTS)
      common /land_r/ ta_LIT_C_T(POINTS), ta_BURN(POINTS)
      common /land_r/ ta_GPP(POINTS,NPFT), ta_NPP(POINTS,NPFT)
      common /land_r/ ta_HT(POINTS,NPFT), ta_LAI(POINTS,NPFT)
      common /land_r/ ta_C_VEG(POINTS,NPFT), ta_LYING_SNOW(POINTS)
      common /land_r/ ta_SURF_ROFF(POINTS), ta_FRAC(POINTS,NTYPE)

! ta_TGND        = time averaged TGND
! ta_SU          = time averaged SU
! ta_SF          = time averaged SF
! ta_PERMA       = time averaged PERMA
! ta_ALBSOIL     = time averaged ALBSOIL
! ta_PFTHICK     = time averaged PFTHICK
! ta_FSMC_PFT    = time averaged FSMC_PFT
! ta_MSLAB       = time averaged ta_MSLAB
! ta_TSLAB       = time averaged ta_TSLAB
! ta_CS_D        = time averaged CS_D
! ta_RESP_SD     = time averaged RESP_SD
! ta_WETMAP      = time averaged ta_WETMAP
! ta_EXCOL       = time averaged ta_EXCOL
! ta_LAYERICE    = time averaged LAYERICE
! ta_FROZENMAP   = time averaged ta_FROZENMAP
! ta_ANN_DWET    = time averaged ta_ANN_DWET
! ta_ACTIVELT    = time averaged ALT
! ta_TALIK       = time averaged TALIK
! ta_SCHDMIN     = time averaged SCHDMIN
! ta_TRACK_PFC   = time averaged TRACK_PFC
! ta_SQU_PFC     = time averaged SQU_PFC

! ta_PGND_CH4D   = time averaged PGND_CH4D (methanogenesis rates ~ regular soil C)
! ta_WET_CH4     = time averaged WET_CH4 (wetland CH4 emissions)

! ta_PGND_CH4P   = time averaged PGND_CH4P (methanogesis rates ~ permafrost C)

      real ta_TGND, ta_SU, ta_SF, ta_PERMA, ta_ALBSOIL, ta_PFTHICK
      real ta_FSMC_PFT, ta_MSLAB, ta_TSLAB, ta_CS_D
      real ta_RESP_SD, ta_WETMAP, ta_EXCOL, ta_LAYERICE
      real ta_FROZENMAP, ta_ANN_DWET, ta_ACTIVELT, ta_TALIK, ta_SCHDMIN
      real ta_TRACK_PFC,ta_SQU_PFC

      real ta_PGND_CH4D
      real ta_WET_CH4

      real ta_PGND_CH4P

      common /land_r/ ta_TGND(POINTS,NGND), ta_SU(POINTS,NGND)
      common /land_r/ ta_SF(POINTS,NGND), ta_PERMA(POINTS)
      common /land_r/ ta_ALBSOIL(POINTS), ta_PFTHICK(POINTS)
      common /land_r/ ta_FSMC_PFT(POINTS,NPFT), ta_MSLAB(POINTS)
      common /land_r/ ta_TSLAB(POINTS), ta_CS_D(POINTS,NGND)
      common /land_r/ ta_RESP_SD(POINTS,NGND), ta_WETMAP(POINTS,3)
      common /land_r/ ta_EXCOL(POINTS), ta_LAYERICE(POINTS,NGND)
      common /land_r/ ta_FROZENMAP(POINTS,7), ta_ANN_DWET(POINTS)
      common /land_r/ ta_ACTIVELT(POINTS), ta_TALIK(POINTS)
      common /land_r/ ta_SCHDMIN(POINTS), ta_TRACK_PFC(POINTS)
      common /land_r/ ta_SQU_PFC(POINTS)

      common /land_r/ ta_PGND_CH4D(POINTS,NGND)
      common /land_r/ ta_WET_CH4(POINTS)

      common /land_r/ ta_PGND_CH4P(POINTS,NGND)

! ta_Zw         = time averaged ZW_bar

      real ta_Zw
      common /land_r/ ta_Zw(POINTS,3)

! ta_CS_P        = time averaged CS_P
! ta_RESP_SP     = time averaged RESP_SP
! ta_AFp         = time averaged AFp

      real ta_CS_P,ta_RESP_SP, ta_AFp
      common /land_r/ ta_CS_P(POINTS,NGND),ta_RESP_SP(POINTS,NGND)
      common /land_r/ ta_AFp(POINTS,NGND)

!-----------------------------------------------------------------------
! time average integrals
!-----------------------------------------------------------------------
! ntatil         = time step counter for time average integrals
! tai_CS         = time averaged integral CS
! tai_RESP_S     = time averaged integral RESP_S
! tai_LIT_C_T    = time averaged integral LIT_C_T
! tai_BURN       = time averaged integral BURN
! tai_CV         = time averaged integral CV
! tai_NPP        = time averaged integral NPP
! tai_GPP        = time averaged integral GPP
! tai_HT         = time averaged integral HT
! tai_LAI        = time averaged integral LAI
! tai_LYING_SNOW = time averaged integral LYING_SNOW
! tai_TSOIL      = time averaged integral TSOIL
! tai_TSTAR      = time averaged integral TSTAR
! tai_M          = time averaged integral M
! tai_ET         = time averaged integral ET
! tai_clnd       = average total carbon in land
! tai_cfa2l      = average total flux atmosphere to land

! tai_WET_CH4    = average total integral WET_CH4
! tai_WET_CH4N40 = average total integral WET_CH4 for wetlands north of 40 N (>= 40 N)
! tai_WET_CH4N45 = average total integral WET_CH4 for wetlands north of 45 N (>= 45 N)
! tai_WET_CH4N60 = average total integral WET_CH4 for wetlands north of 60 N (>= 60 N)
! tai_WET_CH4TROP= average total integral WET_CH4 for tropical wetlands (<30 N and <30 S)

      integer ntatil

      common /land_i/ ntatil

      real tai_CS, tai_RESP_S, tai_LIT_C_T, tai_BURN, tai_CV, tai_NPP
      real tai_GPP, tai_HT, tai_LAI, tai_LYING_SNOW, tai_TSOIL
      real tai_TSTAR, tai_M, tai_ET, tai_clnd, tai_cfa2l

      real tai_WET_CH4, tai_WET_CH4TROP
      real tai_WET_CH4N40, tai_WET_CH4N45, tai_WET_CH4N60

      common /land_r/ tai_CS, tai_RESP_S, tai_LIT_C_T, tai_BURN, tai_CV
      common /land_r/ tai_NPP, tai_GPP, tai_HT, tai_LAI, tai_LYING_SNOW
      common /land_r/ tai_TSOIL, tai_TSTAR, tai_M, tai_ET, tai_clnd
      common /land_r/ tai_cfa2l

      common /land_r/ tai_WET_CH4, tai_WET_CH4TROP
      common /land_r/ tai_WET_CH4N40, tai_WET_CH4N45, tai_WET_CH4N60

! tai_PF45      = time averaged integral PF area >= 45 N
! tai_PFNH      = time averaged integral PF area in N. Hemisphere
! tai_PFGLOBAL  = time averaged integral PF area globally
! tai_WETN45    = time averaged wetland area >= 45 N
! tai_WETNH45SE = time averaged seasonal wetland area >= 45 N
! tai_WETGLOBAL = time averaged wetland area globally
! tai_TRACK_PFC = time averaged integral TRACK_PFC
! tai_SQU_PFC   = time averaged integral SQU_PFC

      real tai_PF45, tai_PFNH, tai_PFGLOBAL,  tai_WET45, tai_WET45SE
      realtai_WETGLOBAL, tai_TRACK_PFC, tai_SQU_PFC

      common /land_r/ tai_PF45, tai_PFNH, tai_PFGLOBAL, tai_WET45
      common /land_r/ tai_WET45SE, tai_WETGLOBAL, tai_TRACK_PFC
      common /land_r/ tai_SQU_PFC

! tai_Zw_bar          = time averaged integral Zw_bar
      real tai_Zw_bar
      common /land_r/ tai_Zw_bar

! tai_PREG_C   = time averaged integral PREG_C
      real tai_PREG_C

      common /land_r/ tai_PREG_C

