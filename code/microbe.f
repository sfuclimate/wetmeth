! source file: /home/miklonzo/Git/WETMETH/updates/microbe.F

      subroutine MICROBE (LAND_PTS,LAND_INDEX, DZ_GND
     &,                   NSOIL, CS_D, STH_SOIL, V_SAT, V_WILT, C_WEIGHT
     &,                   TGND, RESP_S, ZTOP, ZBOT, RESP_SD

     &,                   RESP_SP, CS_P, KAPSp, AFp, PPtm, AF_Ta
     &,                   iAF

     &,                   SAT_FRAC

     &,                   ZMID
     &,                   PGND_CH4D, PGND_CH4P, WET_CH4

     &                     )

!-----------------------------------------------------------------------
! Calculates the soil respiration based on a simplified version of the
! model of Raich et al. (1991).

!**********************************************************************
! this file is based on code that may have had the following copyright:
! (c) CROWN COPYRIGHT 1997, U.K. METEOROLOGICAL OFFICE.

! Permission has been granted by the authors to the public to copy
! and use this software without charge, provided that this Notice and
! any statement of authorship are reproduced on all copies. Neither the
! Crown nor the U.K. Meteorological Office makes any warranty, express
! or implied, or assumes any liability or responsibility for the use of
! this software.
!**********************************************************************

!-----------------------------------------------------------------------

      implicit none

      include "size.h"

! POINTS     = IN Total number of land points.
! LAND_PTS   = IN Number of points on which TRIFFID may operate.
! LAND_INDEX = IN Indices of land points on which TRIFFID may operate.

      integer LAND_PTS, LAND_INDEX(POINTS), I, L
!# if defined 1
!      integer ZN, ZK
!# endif

! CS         = IN Soil carbon (kg C/m2).
! STH_SOIL   = IN Top layer soil moisture as a fraction of saturation
!              (m3/m3).
! V_SAT      = IN Volumetric soil moisture concentration at saturation
!              (m3 H2O/m3 soil).
! V_WILT     = IN Volumetric soil moisture concentration below which
!              stomata close as a fraction of saturation
!              (m3 H2O/m3 soil).
! TSOIL      = IN Soil temperature (K).
! RESP_S     = OUT Soil respiration (kg C/m2/s).
! FSTH,FTEMP = WORK Factors describing the influence of soil moisture
!              and soil temperature respectively on soil respiration.
! STH_OPT    = WORK Fractional soil moisture at which respiration is
!              maximum.
! STH_WILT   = WORK Wilting soil moisture as a fraction of saturation.

      integer NSOIL(POINTS), N

! CS_D       = IN Soil carbon (kg C/m3).
! TRESP      = IN Carbon weighted soil temperature (K).
! TGND       = IN Soil temperature (K)
! RESP_SD    = OUT Soil respiration layer (kg C/m3/s).

      real CS_D(POINTS,NGND), STH_SOIL(POINTS, NGND)
      real V_SAT(POINTS,NGND)
      real V_WILT(POINTS, NGND)
      real TGND(POINTS, NGND), RESP_S(POINTS), FSTH(NGND), FTEMP(NGND)
      real STH_OPT, STH_WILT, ZBOT(NGND), ZTOP(NGND)
      real C_WEIGHT(POINTS,NGND), DZ_GND(NGND), RESP_SD(POINTS,NGND)

! RESP_SP    = OUT permafrost carbon respiration layer (kg C/m3/s).
! KAPSp = Specific soil respiration rate at 25 deg and optimum soil
!        moisture (/s) for permafrost carbon.
! Q10p  = Q10p factor for permafrost carbon respiration.
! pFTEMP = Permafrost FTEMP
! CS_P = Depth varying permafrost carbon density (kg m-3)
! PPtm = passive pool transmutation rate
! AFp = Available fraction permafrost carbon
! AF_Ta = Available fraction transmutation rate

      real RESP_SP(POINTS,NGND),KAPSp, Q10p,pFTEMP,CS_P(POINTS,NGND)
      real PPtm, AFp(POINTS,NGND), AF_Ta(POINTS,NGND), iAF
      parameter (Q10p=2.0)

! SAT_FRAC      = fraction of soil layer saturated with water ()
! ZMID          = depth to the mid-point of the soil layer (m)
! CtoCH4        = specific methanogenesis rate (kg kg-1 s-1), which can be
!                 thought as the mass fraction of CH4 produced per
!                 mass of (decomposed) soil C. Treat et al. 2015 (BGC)
!                 suggests that this fraction can range from 0.3 to 27.2
!                 x 10-6 g of CH4-C per g of soil C per day in different
!                 landscapes across the northern high-latitudes (>50N)
!                 depending on the soil depth and relative water table position.
! Q10prod       = constant Q10 coefficient for methanogenesis ()
! Q10prod_TEMP  = temperature-dependent Q10 coefficient for methanogenesis ()
!                 peak-and-decline Q10 values with an optimal temperature
!                 between 25-30 deg. Celsius (Dunfield 1993; Dean et al. 2018)
! FTEMP_CH4     = temperature-dependency function for methanogenesis ()
! RZ_CH4        = depth-control function on methanogenesis ()
! TAU_Prod      = scaling parameter for methanogenesis (m)
! Z_Oxic        = thickness(es) of the oxic zone (m)
! TAU_Oxid      = scaling parameter for methanotrophy (m)
! Frac_Oxid     = fraction of CH4 consumed before release based on the
!                 of water-saturation level in the soil column ()
! PGND_CH4D     = methanogenesis rates per non-permafrost soil layer (kg C/m3/s)
! PGND_CH4P     = methanogenesis rates per permafrost layer (kg C/m3/s)
! PTOT_CH4D     = total amount of CH4 produced from non-permafrost C
!                 in the soil column (kg C/m2/s)
! PTOT_CH4P     = total amount of CH4 produced from permafrost C
!                 in the soil column (kg C/m2/s)
! OTOT_CH4D     = total amount of CH4 consumed from non-permafrost C
!                 in the soil column (kg C/m2/s)
! OTOT_CH4P     = total amount of CH4 consumed from permafrost C
!                 in the soil column (kg C/m2/s)
! WET_CH4       = total amount of CH4 released out of a grid cell (kg C/m2/s)
!                 ~ the grid cell may or may not be underlain by permafrost

      real SAT_FRAC(POINTS,SCHDMAX)

      real ZMID(NGND), Z_Oxic(SCHDMAX)
      real CtoCH4, Q10prod

      real Q10prod_TEMP(POINTS,NGND)

      real RZ_CH4(NGND), FTEMP_CH4(NGND)
      real TAU_Prod, TAU_Oxid
      real Frac_Oxid(POINTS)
      real PGND_CH4D(POINTS,NGND), PGND_CH4P(POINTS,NGND)
      real PTOT_CH4D(POINTS), PTOT_CH4P(POINTS)
      real OTOT_CH4D(POINTS), OTOT_CH4P(POINTS)
      real WET_CH4(POINTS)
      parameter(CtoCH4=22.8*1e-6/86400)
      parameter(Q10prod=4.2)
      parameter(TAU_Prod=0.75)
      parameter(TAU_Oxid=0.0146)
      DATA Z_Oxic /0.05, 0.15, 0.32, 0.61, 1.11, 1.96/

! Local parameters
! KAPS = Specific soil respiration rate at 25 deg and optimum soil
!        moisture (/s).
! Q10  = Q10 factor for soil respiration.

      real KAPS, Q10
      parameter (KAPS=0.35E-8, Q10=2.0)

! the above flag/option assumes that the
! flag/option 1 is "ON"

!-------------------------------------------
! initializing key wetland methane variables
!-------------------------------------------
      PGND_CH4D(:,:)=0.
      PTOT_CH4D(:)=0.
      OTOT_CH4D(:)=0.
      WET_CH4(:)=0.

      PGND_CH4P(:,:)=0.
      PTOT_CH4P(:)=0.
      OTOT_CH4P(:)=0.
      !WET_CH4(:)=0. ~ same WET_CH4 for both non-PF C and PF C

      RESP_SD(:,:) = 0.
      RESP_S(:)=0.0

      RESP_SP(:,:) =0.
      AF_Ta(:,:) =0.

      do I=1,LAND_PTS
        L = LAND_INDEX(I)

        FSTH(:)= 0.0
	FTEMP(:) = 0.0

        do N=1,NGND
          if (V_SAT(L,N) .gt. 0.0) then
            STH_WILT = V_WILT(L,N) / V_SAT(L,N)
            STH_OPT = 0.5 * (1 + STH_WILT)
            if (STH_SOIL(L,N) .le. STH_WILT) then
              FSTH(N) = 0.2
            elseif (STH_SOIL(L,N) .gt. STH_WILT .and.
     &              STH_SOIL(L,N) .le. STH_OPT) then
              FSTH(N) = 0.2 + 0.8*((STH_SOIL(L,N) - STH_WILT)
     &               /(STH_OPT - STH_WILT))
            elseif (STH_SOIL(L,N) .gt. STH_OPT) then
              FSTH(N) = 1 - 0.8 * (STH_SOIL(L,N) - STH_OPT)
            endif
            if(TGND(L,N) .ge. 273.15) then
	      FTEMP(N) = Q10 ** (0.1 * (TGND(L,N) - 298.15))
            else
	      FTEMP(N)=0.0
	    endif
            RESP_SD(L,N) = KAPS* CS_D(L,N) * FSTH(N) * FTEMP(N)

              if(AFp(L,N) .eq. 0. .and. CS_P(L,N) .gt. 0.)then
	        AFp(L,N)=iAF
	      endif

              if(TGND(L,N) .ge. 273.15) then
		pFTEMP = Q10p ** (0.1 * (TGND(L,N) - 298.15))
              else
	        pFTEMP=0.0
	      endif

              RESP_SP(L,N) = KAPSp*CS_P(L,N)*AFp(L,N) * FSTH(N) *pFTEMP

!
	      AF_Ta(L,N) = PPtm*CS_P(L,N)*(1-AFp(L,N)) * FSTH(N) *pFTEMP

          else
            RESP_SD(L,N) = 0.

            RESP_SP(L,N) = 0.0

          endif
	  RESP_S(L)=RESP_S(L)+(RESP_SD(L,N)*DZ_GND(N))

          RESP_S(L)=RESP_S(L)+(RESP_SP(L,N)*DZ_GND(N))

        enddo

!------------------------------------------
!~~~ STARTING WETLAND METHANE PROCESSES ~~~
!------------------------------------------

!-------------------------------
! 1. METHANOGENESIS IN WETLANDS
!-------------------------------
       !------------------------------------------------------
       ! estimating methanogenesis rates per soil layer and
       ! determining total methanogenesis in the soil column
       !------------------------------------------------------
          do N=1,NGND
             ! -----------------------------------------------
             ! first, temperature-dependency function for methane
             ! production.
             ! -----------------------------------------------
             if (TGND(L,N) .gt. 273.15) then
             ! if the soil layer is unfrozen

             ! defining a temperature-dependent Q10 coefficient such that
             ! it peaks and declines after an optimal temperature between
             ! 25-30 deg. Celsius (Dunfield et al. 1993; Dean et al. 2018)
                Q10prod_TEMP(L,N)=1.7+2.5*tanh(0.1*(308.15-TGND(L,N)))
             ! making sure the Q10 coefficient is non-negative
                if (Q10prod_TEMP(L,N) .lt. 0.0) then
                    Q10prod_TEMP(L,N) = 1e-3
                endif
             ! then determining the corresponding temperature-dependecy
             ! function for methanogenesis
                FTEMP_CH4(N) = Q10prod_TEMP(L,N)
     &                           **(0.1*(TGND(L,N)-273.15))

             else
             ! if the soil layer is frozen
                FTEMP_CH4(N) = 0
             endif

             !------------------------------------------------
             ! second, depth-control function on methanogenesis
             ! as an exponential decay with depth
             !------------------------------------------------
             RZ_CH4(N) = exp(-ZMID(N)/TAU_Prod)

             !------------------------------------------------
             ! finally, methanogenesis rates in soil layers
             !------------------------------------------------
             ! methanogenesis rates from non-permafrost C (in kg C/m3/s)

             PGND_CH4D(L,N) = CtoCH4*CS_D(L,N)*SAT_FRAC(L,N)
     &                           *FTEMP_CH4(N)*RZ_CH4(N)
             ! making sure there is no negative methanogenesis rate
             ! (due to potential negative CS_D(L,N))
             if (PGND_CH4D(L,N) .lt. 0.0) then
                 PGND_CH4D(L,N) = 0.0
             endif

             ! methanogenesis rates from permafrost C (in kg C/m3/s)
             PGND_CH4P(L,N) = CtoCH4*CS_P(L,N)*SAT_FRAC(L,N)
     &                           *FTEMP_CH4(N)*RZ_CH4(N)
             ! making sure there is no negative methanogenesis rate
             if (PGND_CH4P(L,N) .lt. 0.0) then
                 PGND_CH4P(L,N) = 0.0
             endif

             !-------------------------------------------------
             ! then, integrate over depth: total methanogenesis
             !-------------------------------------------------
             ! methanogenesis rates integrated over the soil
             ! column (in kg C/m2/s)
             PTOT_CH4D(L) = PTOT_CH4D(L)+(PGND_CH4D(L,N)*DZ_GND(N))

             PTOT_CH4P(L) = PTOT_CH4P(L)+(PGND_CH4P(L,N)*DZ_GND(N))

          enddo

!       endif

!-----------------------------
! 2. METHANOTROPHY IN WETLANDS
!-----------------------------

!-------------------------------------------------------
! check saturated layers from bottom to top, then
! calculate an estimated rate for total methanotrophy
! as a fraction of the total methanogenesis rate ~
! the deeper the first saturated layer, the higher the
! methanotrophy rate
!-------------------------------------------------------

          if (SAT_FRAC(L,6) .gt. 0.0 .and.
     &        SAT_FRAC(L,5) .eq. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(6)/TAU_Oxid)
          else if (SAT_FRAC(L,5) .gt. 0.0 .and.
     &             SAT_FRAC(L,4) .eq. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(5)/TAU_Oxid)
          else if (SAT_FRAC(L,4) .gt. 0.0 .and.
     &             SAT_FRAC(L,3) .eq. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(4)/TAU_Oxid)
          else if (SAT_FRAC(L,3) .gt. 0.0 .and.
     &             SAT_FRAC(L,2) .eq. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(3)/TAU_Oxid)
          else if (SAT_FRAC(L,2) .gt. 0.0 .and.
     &             SAT_FRAC(L,1) .eq. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(2)/TAU_Oxid)
          else if (SAT_FRAC(L,1) .gt. 0.0) then
             Frac_Oxid(L) = 1-exp(-Z_Oxic(1)/TAU_Oxid)
          else
             Frac_Oxid(L) = 1.00
          endif

          ! methanotrophy rates for the entire grid box
          ! (in kg C/m2/s)
          OTOT_CH4D(L) = PTOT_CH4D(L)*Frac_Oxid(L)

          ! for mass conservation: oxidized methane
          ! contributes to aerobic respiration as CO2
          ! (molar ratio: 1 mol of CH4-C => 1 mol of CO2-C)
          RESP_S(L) = RESP_S(L) + OTOT_CH4D(L)

          OTOT_CH4P(L) = PTOT_CH4P(L)*Frac_Oxid(L)

          ! for mass conservation: oxidized methane
          ! contributes to aerobic respiration as CO2
          ! (molar ratio: 1 mol of CH4-C => 1 mol of CO2-C)
          RESP_S(L)= RESP_S(L) + OTOT_CH4P(L)

!------------------------------------
! 3. METHANE EMISSIONS FROM WETLANDS
!------------------------------------

!----------------------------------------------
! released methane is assumed to be the balance
! between total produced and total consumed
! methane (no storage at the moment)
!----------------------------------------------

          ! Here, the methane release is to be passed to the
          ! atmosphere. Hence, the same variable name (WET_CH4)
          ! is used for methane from both non-permafrost and
          ! permafrost C.

          ! rates of methane release from a methane producing grid cell
          ! (in kg C/m2/s). The methane released from a grid cell is the
          ! balance between produced and consumed methane
          WET_CH4(L) = WET_CH4(L) + (PTOT_CH4D(L)-OTOT_CH4D(L))

          WET_CH4(L) = WET_CH4(L) + (PTOT_CH4P(L)-OTOT_CH4P(L))

!-----------------------------------------------
! ~~~ CLOSING THE WETLAND METHANE PROCESSES ~~~
!-----------------------------------------------

      enddo

      return
      end
