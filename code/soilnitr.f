! source file: /home/miklonzo/Git/WETMETH/updates/soilnitr.F
      subroutine SOILNITR (POINTS, LAND_PTS, LAND_INDEX, FORW, GAMMA
     &,    DENOM_MIN, LIT_N_T, N_RESP_S_D, N_RESP_L, N_HUMIF, NS_D, NL
     &,    IMM_NH4, IMM_NO3, TAU_L
     &,    CS_PART, SCHDMAX, FZN_SL, DZ_GND, NGND, NSo)

!----------------------------------------------------------------------
! Updates carbon contents of the soil.

!**********************************************************************
! this file is based on code that may have had the following copyright:
! (c) CROWN COPYRIGHT 1997, U.K. METEOROLOGICAL OFFICE.

! Permission has been granted by the authors to the public to copy
! and use this software without charge, provided that this Notice and
! any statement of authorship are reproduced on all copies. Neither the
! Crown nor the U.K. Meteorological Office makes any warranty, express
! or implied, or assumes any liability or responsibility for the use of
! this software.
!**********************************************************************
!----------------------------------------------------------------------

      implicit none

! POINTS     = IN Total number of land points.
! LAND_PTS   = IN Number of points on which TRIFFID may operate.
! LAND_INDEX = IN Indices of land points on which TRIFFID may operate.

      integer POINTS, LAND_PTS, LAND_INDEX(POINTS), L, T, K, N

! FORW       = IN Forward timestep weighting.
! GAMMA      = IN Inverse timestep (/360days).
! DENOM_MIN  = IN Minimum value for the denominator of the update
!              equation. Ensures that gradient descent does not lead
!              to an unstable solution.
! N_RESP_S_D = INOUT Soil nitrogen decomposition (kg N/m2/360days).
! N_RESP_L   = INOUT Litter nitrogen decomposition (kg N/m2/360days).
! N_HUMIF    = INOUT Transfer of N from litter to soil (kg N/m2/360days).
! NS         = INOUT Soil nitrogen (kg N/m2).
! NL         = INOUT Litter nitrogen (kg N/m2).
! DNS        = WORK Increment to the soil nitrogen (kg N/m2).
! DNL        = WORK Increment to the litter nitrogen (kg N/m2).
! DNH4       = WORK Increment to the ammonium nitrogen (kg N/m2).
! DNO3       = WORK Increment to the nitrate nitrogen (kg N/m2).
! DPS_DNS    = WORK Rate of change of PS with soil nitrogen (/360days).
! DPL_DNL    = WORK Rate of change of PL with litter nitrogen (/360days).
! DPNH4_DNH4 = WORK Rate of change of PNH4 with ammonium nitrogen (/360days).
! DPNO3_DNO3 = WORK Rate of change of PNO3 with nitrate nitrogen (/360days).
! PS         = WORK Net nitrogen accumulation in the soil
!              (kg N/m2/360days).
! PL         = WORK Net nitrogen accumulation in the litter
!              (kg N/m2/360days).
! PNH4       = WORK Net nitrogen accumulation in the ammonium pool
!              (kg N/m2/360days).
! PNO3       = WORK Net nitrogen accumulation in the nitrate pool
!              (kg N/m2/360days).
! TAU_L   = Fraction of transfer of litter to soil carbon pool, i.e. humification.
! IMM_NH4    = OUT Immobilisation of ammonium (kg N/m2/360days).
! IMM_NO3    = OUT Immobilisation of nitrate (kg N/m2/360days).
! SCHDMAX    = IN maximum depth of active soil carbon
! FZN_SL     = IN frozen soil layers (layer #)
! DZ_GND     = IN thickness of each soil layer (m)
! CS_PART    = IN partitioning of Litter into subsurface layers
! IFZN       = WORK the integer form of FZN_SL
! NINPUT     = WORK humus chaned to triffid time-step
! NSo             = OUT Soil nitrogen (kg N/m2).

      integer IFZN(POINTS), NGND, SCHDMAX

      real FORW, GAMMA, LIT_N_T(POINTS), DENOM_MIN
      real NS_D(POINTS,NGND), DNS(POINTS), DPS_DNS(POINTS), PS(POINTS)
      real NL(POINTS), DNL(POINTS), DPL_DNL(POINTS), PL(POINTS)
      real DNH4(POINTS), DPNH4_DNH4(POINTS)
      real DNO3(POINTS), DPNO3_DNO3(POINTS)
      real N_RESP_S_D(POINTS,NGND), N_RESP_L(POINTS)
      real N_HUMIF(POINTS),PNH4(POINTS),PNO3(POINTS)
      real UP_NH4(POINTS), UP_NO3(POINTS)
      real IMM_NH4(POINTS,NGND), IMM_NO3(POINTS,NGND)
      real TAU_L
      real TEST,NINPUT(POINTS)
      real DZ_GND(NGND), CS_PART(SCHDMAX+1,NGND),FZN_SL(POINTS)
      real NSo(POINTS)

      do T=1,LAND_PTS
        L=LAND_INDEX(T)

!----------------------------------------------------------------------
! Diagnose the net local nitrogen flux into the litter
!----------------------------------------------------------------------

        PL(L) = LIT_N_T(L) - N_RESP_L(L) - N_HUMIF(L)

!----------------------------------------------------------------------
! Variables required for the implicit and equilibrium calculations
!----------------------------------------------------------------------

        DPL_DNL(L) = (N_RESP_L(L) + N_HUMIF(L)) / NL(L)

!----------------------------------------------------------------------
! Save current value of soil nitrogen
!----------------------------------------------------------------------

        DNL(L) = NL(L)

      enddo

!----------------------------------------------------------------------
! Update litter nitrogen
!----------------------------------------------------------------------
      TEST = 1
      call NDECAY (POINTS, LAND_PTS, LAND_INDEX, DPL_DNL, FORW, GAMMA
     &     ,DENOM_MIN, PL, NL, TEST)

!----------------------------------------------------------------------
! Apply implicit correction to the litter and soil respiration rate.
!----------------------------------------------------------------------

      do T=1,LAND_PTS
        L=LAND_INDEX(T)

        DNL(L) = NL(L) - DNL(L)
        N_RESP_L(L) = N_RESP_L(L) + FORW*DPL_DNL(L)*DNL(L) * (1-TAU_L)
        N_HUMIF(L) = N_HUMIF(L) + FORW*DPL_DNL(L)*DNL(L) * TAU_L

      enddo
!----------------------------------------------------------------------
! AHMD Remodel fluxes to resemble deep soil carbon
!----------------------------------------------------------------------
      IFZN(:) = INT(FZN_SL(:))

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!----------------------------------------------------------------------
! Update soil organic nitrogen
!----------------------------------------------------------------------
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!----------------------------------------------------------------------
! Update organic soil nitrogen pool
!----------------------------------------------------------------------

!   ~Partition new nitrogen~
!   Nitrogen is partitioned acording to CS_PART weighted toward surface
!   Organic nitrogen cannot be partitioned into frozen soil layers

      do T=1,LAND_PTS
        L=LAND_INDEX(T)

!       change litter to triffid time-step
	NINPUT(L)=N_HUMIF(L)/GAMMA
	if(IFZN(L) .eq. 0) then
	   NS_D(L,1)=NS_D(L,1)+((NINPUT(L)*CS_PART(1,1))/(DZ_GND(1)))
	else
	  do K=1,IFZN(L)
	     NS_D(L,K)=NS_D(L,K)
     &	     +(NINPUT(L)*CS_PART((IFZN(L)+1),K))/(DZ_GND(K))
	  enddo
	endif

!      ~Remove Nitrogen that has been respired, Add immobalized N~
        do N=1,SCHDMAX
	   NS_D(L,N)=NS_D(L,N)-(N_RESP_S_D(L,N)/GAMMA)
     &	            +(IMM_NH4(L,N)/GAMMA)
     &      	    +(IMM_NO3(L,N)/GAMMA)
        enddo

!     ~ Sum-up 2-D soil organic nitrogen pool ~
        NSo(L)= 0.0
	do N=1,SCHDMAX
	   NSo(L) = NSo(L) + NS_D(L,N)*DZ_GND(N)
	enddo

!      if(L .eq. 2772)then
!        write(*,*) ''
!	write(*,*) 'Soil N conserve 2'
!	write(*,*) 'N_HUMIF(L)', N_HUMIF(L)
!	write(*,*) 'NINPUT(L)', NINPUT(L)
!	write(*,*) 'NSo', NSo(L)
!	write(*,*) 'NL(L)', NL(L)
!	stop
!      endif

      enddo ! enddo for points loop
      return
      end

      subroutine NDECAY (POINTS, LAND_PTS, LAND_INDEX, DF_DP, FORW
     &     ,GAMMA, DENOM_MIN, FLUX, POOL,TEST)

!-----------------------------------------------------------------------
! Updates nitrogen contents of the soil.

**********************************************************************
! this file is based on code that may have had the following copyright:
! (c) CROWN COPYRIGHT 1997, U.K. METEOROLOGICAL OFFICE.

! Permission has been granted by the authors to the public to copy
! and use this software without charge, provided that this Notice and
! any statement of authorship are reproduced on all copies. Neither the
! Crown nor the U.K. Meteorological Office makes any warranty, express
! or implied, or assumes any liability or responsibility for the use of
! this software.
!**********************************************************************

!-----------------------------------------------------------------------
      implicit none

! POINTS     = IN Total number of land points.
! LAND_PTS   = IN Number of points on which TRIFFID may operate.
! LAND_INDEX = IN Indices of land points on which TRIFFID may operate.

      integer POINTS, LAND_PTS, LAND_INDEX(POINTS), L, T

! DF_DP      = IN Rate of change of FLUX with POOL (yr).
! FORW       = IN Forward timestep weighting.
! GAMMA      = IN Inverse timestep (/360days).
! DENOM_MIN  = IN Minimum value for the denominator of the update
!              equation. Ensures that gradient descent does not lead
!              to an unstable solution.
! FLUX       = IN Net flux into the pool (kg N/m2/360days).
! POOL       = INOUT nitrogen pool (kg N/m2).
! DENOM      = WORK Denominator of update equation.
! NUMER      = WORK Numerator of the update equation.
! POOL_MIN     = Minimum nitrogen pool (kg N/m2).

      real DF_DP(POINTS)
      real FORW, GAMMA, DENOM_MIN, FLUX(POINTS)
      real POOL(POINTS), DENOM, NUMER, POOL_MIN, TEST
      parameter (POOL_MIN=1.0E-12)

      do T=1,LAND_PTS
        L=LAND_INDEX(T)

        NUMER = FLUX(L)
        DENOM = GAMMA+FORW*DF_DP(L)
        DENOM = MAX(DENOM,DENOM_MIN)
        POOL(L) = POOL(L)+NUMER/DENOM
        POOL(L) =POOL(L)
        POOL(L) = MAX(POOL_MIN,POOL(L))
        IF (TEST.eq.3) then
           IF (POOL(L).LT.0) print*, 'POOL', L, POOL(L), TEST
      endif
      enddo

      return
      end
