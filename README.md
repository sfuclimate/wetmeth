# Description of the WETMETH 1.0 code as embedded in the UVic ESCM

WETMETH 1.0 is a wetland methane model developed for implementation in Earth system models.

The shared code is for the frozen ground version of the UVic ESCM 2.9 - an Earth system model
of intermediate complexity (http://terra.seos.uvic.ca/model/) - in which WETMETH 1.0 has been embedded.
The core of WETMETH is in the microbe subroutine. Key subroutines for the model outputs are mtlmio,
mtlm_tavg and mtlm_tsi.

For further information, please contact Claude-Michel Nzotungicimpaye at cnzotung-at-sfu.ca.
